/*
  Exploring particle effects given the material covered to date.  
*/

//For exit()
#include <stdlib.h>

//For printf()
#include <stdio.h>

// For round()
#include <math.h>

// For time()
#include <time.h>

#if defined(_WIN32) || defined (_WIN64)
    //The SDL library
    #include "SDL.h"
    //Support for loading different types of images.
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_image.h"
#endif

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;


const int SDL_OK = 0;

//For particle animation 
const int MAX_PARTICLE_ANIMATION_FRAMES = 60;
typedef struct {
    int lifetime;
    float xVel;
    float yVel;
    float xPos;
    float yPos;
    SDL_Rect target;
    SDL_Rect fireFrames[MAX_PARTICLE_ANIMATION_FRAMES];
    SDL_Texture* fireTexture;
    int currentFrame;
} Particle;

int main( int argc, char* args[] )
{
     // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;


    // Player Image - source rectangle
    // - The part of the texture to render.
    // - Note: This is an SDL structure!!
    const int FIRE_SPRITE_HEIGHT = 64;
    const int FIRE_SPRITE_WIDTH = 64;

 
    // Window control 
    SDL_Event event;
    bool quit = false;  //false

    // Fire particles
    // Note - 1000 partcles causes a stack overflow on windows. 
    const int MAX_PARTICLES = 100;
    Particle particles[MAX_PARTICLES];
    SDL_Texture* particleTexture; // only one copy of this

    // Timing variables
    unsigned int currentTimeIndex;
    unsigned int prevTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;

    float accumulator;

    srand(time(NULL));

    // SDL allows us to choose which SDL components are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
    /**********************************
     *    Setup fire image     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/fire2_64.png");

    if(temp == nullptr)
    {
        printf("Fire image not found!");
    }

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    particleTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'temp' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;

    float speed;
    //setup particles
    for(int p = 0; p < MAX_PARTICLES; p++)
    {
        Particle *current = &particles[p]; // get a pointer to the pth element
                                          // easier to work with. 

        current->fireTexture = particleTexture;
        current->target.h = FIRE_SPRITE_HEIGHT;
        current->target.w = FIRE_SPRITE_WIDTH;

        // random vector for velocity
        current->xVel = ((rand() / (1.0f*RAND_MAX)) * 2.0f) - 1.0f;
        current->yVel = ((rand() / (1.0f*RAND_MAX)) * 2.0f) - 1.0f;

        //normalise 
        float length = sqrt(pow(current->xVel,2.0f) + pow(current->yVel,2.0f));
        current->xVel /= length;
        current->yVel /= length;

        //scale by speed
        speed = 1.0f * (rand() % 100) + 30.0f;
        current->xVel *= speed; 
        current->yVel *= speed;

        current->xPos = 250.0f;
        current->yPos = 250.0f;

        current->lifetime = 500+(rand() % 2000);

        //random start frame
        current->currentFrame = rand() % MAX_PARTICLE_ANIMATION_FRAMES;

        int fireFramesIndex = 0;

        //Setup animation frames
        for(int rows = 0; rows < 6; rows++)
        {
            for(int cols = 0; cols < 10; cols++)
            {
                fireFramesIndex = (rows*10)+cols;
                current->fireFrames[fireFramesIndex].x = (cols * FIRE_SPRITE_WIDTH); //ith col.
                current->fireFrames[fireFramesIndex].y = (rows * FIRE_SPRITE_HEIGHT); //3rd row. 
                current->fireFrames[fireFramesIndex].w = FIRE_SPRITE_WIDTH;
                current->fireFrames[fireFramesIndex].h = FIRE_SPRITE_HEIGHT;
            }
        }
    }


    //zero frame time accumulator
    accumulator = 0.0f;

    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();


     // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist - https://gafferongames.com/post/fix_your_timestep/

        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Store animation time delta
        accumulator += timeDeltaInSeconds;

        // Handle input 

        if( SDL_PollEvent( &event ))  // test for events
        { 
            switch(event.type) 
            { 
                case SDL_QUIT:
    		        quit = true;
    		    break;

                // Key pressed event
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    }
                break;

                // Key released event
                case SDL_KEYUP:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        //  Nothing to do here.
                        break;
                    }
                break;
                
                default:
                    // not an error, there's lots we don't handle. 
                    break;    
            }
        }

        
        //update particles
        for(int p = 0; p < MAX_PARTICLES; p++)
        {
            Particle *current = &particles[p]; // get a pointer to the pth element
                                          // easier to work with. 


            float xMovement = timeDeltaInSeconds * current->xVel;
            float yMovement = timeDeltaInSeconds * current->yVel;
            

            // Update player position.
            current->xPos+=xMovement;
            current->yPos+=yMovement;

            // Move sprite to nearest pixel location.
            current->target.x = round(current->xPos);
            current->target.y = round(current->yPos);
            
            //age particle
            current->lifetime-=timeDeltaInSeconds;
        }

        // Check if animation needs update
        if(accumulator > 0.0417f) //24fps
        {
            accumulator = 0.0f;

            for(int p = 0; p < MAX_PARTICLES; p++)
            {
                Particle *current = &particles[p]; // get a pointer to the pth element
                                                    // easier to work with. 
                current->currentFrame++;
                if(current->currentFrame >= MAX_PARTICLE_ANIMATION_FRAMES)
                {
                    current->currentFrame = 0;
                }
            }
        
        }

        //Draw stuff here.

        // 1. Clear the screen
        SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
        // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

        SDL_RenderClear(gameRenderer);

        // 2. Draw the scene - i.e. all the particles

        float angle;

        for(int p = 0; p < MAX_PARTICLES; p++)
        {
            Particle *current = &particles[p]; // get a pointer to the pth element       
                                             // easier to work with. 

            if(current->lifetime > 0.0f) // draw only live particles. 
            {
                angle = atan2(current->yVel,current->xVel);
                angle = angle * (180.0f/3.142);
                angle -= 90.0f;
                

                SDL_RenderCopyEx(gameRenderer,
                                current->fireTexture, 
                                &current->fireFrames[current->currentFrame], 
                                &current->target,
                                angle,
                                NULL,
                                SDL_FLIP_NONE);

            }

        }

        // 3. Present the current frame to the screen
        SDL_RenderPresent(gameRenderer);

    }
    
    //Clean up!
    SDL_DestroyTexture(particleTexture);
    particleTexture = nullptr;   

    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   

    //Shutdown SDL - clear up resources etc.
    SDL_Quit();

    exit(0);
}


